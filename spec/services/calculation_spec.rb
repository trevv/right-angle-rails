require "rails_helper"

RSpec.describe Calculation do

  before(:all) do
    @stamp = build(:stamp)
    @state = build(:state)
    @city = build(:city)
    @user = build(:user)
    @job = build(:job)
    @company = build(:company)
  end

  # it "loads in the state right" do
  #   expect(@state).to be_valid
  # end
  # it "loads in the stamp right" do
  #   expect(@stamp).to be_valid
  # end
  # it "loads in the city right" do
  #   expect(@city).to be_valid
  # end
  # it "loads in the job right" do
  #   expect(@job).to be_valid
  # end
  # it "loads in the user right" do
  #   expect(@user).to be_valid
  # end
  # it "loads in the company right" do
  #   expect(@company).to be_valid
  # end
  it "can reference calculations from here" do
    expect(Calculation.say_hello).to eq("YO!")
  end

  it "returns flat roofs 1 correctly" do
    expect(Calculation.flat_roofs_1(@city.ground_snow_load_pressure)).to eq(69.30000000000001)
  end

  it "returns the appropriate roof_material_psf" do
    result = Calculation.roof_material_psf(@job.roof_material)
    case @job.roof_material
    when "ashpalt shingles"
      expect(result).to eq(2)
    when "corrugated metal"
      expect(result).to eq(3)
    when "concrete tiles"
      expect(result).to eq(12)
    when "clay tiles"
      expect(result).to eq(12)
    when "TPO membrane"
      expect(result).to eq(1.5)
    when "gravel"
      expect(result).to eq(6)
    when "spanish tiles"
      expect(result).to eq(19)
    end
  end

end