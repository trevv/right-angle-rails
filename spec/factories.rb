FactoryBot.define do
  factory :connection_type do
    name { "MyString" }
    shear { 1.5 }
    pullout { 1.5 }
    active { false }
  end

  factory :state do
    name {"Utah"}
    abbrev {"UT"}
    elevation {4500}
    ground_snow_load_pressure {100}
    design_wind_speed {50}
    id {100}
    ibc1 {"UTAH IBC1"}
    ibc2 {"UTAH IBC2"}
    iebc  {"UTAH IEBC"}
    iebc2  {"UTAH IEBC2"}
  end
  factory :city do
    id {100}
    name {"Lehi"}
    elevation {4500}
    ground_snow_load_pressure {100}
    design_wind_speed {50}
    frost_depth {20}
    wind_load {100}
    association :state, factory: :state, strategy: :build
    association :stamp, factory: :stamp, strategy: :build
  end
  factory :company do
    id {100}
    name {"Big Bright Light Company"}
    state {"Utah"}
    city {"Lehi"}
    stamp_on_CAD {true}
    stamp_pages {[1,2,3]}
    active {true}
  end

  factory :job do
    send_pdf {false}
    is_premanufacture_truss {false}
    email {"sendtestemailshere@gmail.com"}
    solar_company_name {"Big Bright Light Co."}
    client_last_name {"Johnson"}
    street_address {"2068 N Hiddencreek Dr"}
    city_name {"Lehi"}
    state {"Utah"}
    zip {84043}
    roof_square_footage {975}
    home_elevation {4350}
    number_of_panels {10}
    roof_pitch {23.4}
    structure_type {"residence"}
    number_of_arrays {2}
    average_ballast_psf {1.75}
    roof_material {"clay tiles"}
    roof_framing_type {"2x4 rafters"}
    rafter_truss_span {2}
    rafter_truss_spacing {3}
    number_of_footing {3}
    pole_size {"10"}
    association :company, factory: :company, strategy: :build
    association :user, factory: :user, strategy: :build

  end
  factory :stamp do
    id {100}
    name {"Utah Stamp"}
    file_path {"some/file/path.jpf"}
  end
  factory :user do
    email {"tester@anemail.com"}
    name {"Johnny Tester"}
    password {"password"}
    association :company, factory: :company, strategy: :build
  end
end