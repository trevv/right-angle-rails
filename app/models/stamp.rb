class Stamp < ApplicationRecord
  belongs_to :state
  has_many :cities
  has_one_attached :image

  scope :active, -> { where("active=TRUE")}
end
