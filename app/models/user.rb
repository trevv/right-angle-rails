class User < ApplicationRecord
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  belongs_to :company, optional: true

  has_many :jobs

  def set_default_role
    self.role ||= :user
  end

  def associate_with_company(company)
    self.company_id = company
    self.save!
  end

  def first_name
    self.name.partition(" ").first
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
end
