class City < ApplicationRecord

  scope :detect_by_name, -> (city_name, state_abbrev) { joins(:state).where("cities.name = ? AND states.abbrev = ? ", city_name, state_abbrev)}
  include SearchCop

  search_scope :search do
    attributes :name
  end

  has_many :jobs
  has_many :companies
  has_many :conditional_ground_snow_loads
  belongs_to :stamp, optional: true
  belongs_to :letter_type, optional: true
  belongs_to :state

  validates :name,
            :state_id,
            presence: true


  def self.available_states
    distinct.pluck(:state_abbreviation)
  end

  def is_missing_required_fields
    if self.ibc.nil? || self.iebc.nil? || self.ground_snow_load_pressure.nil? || self.wind_load.nil? || self.asce.nil?
      return true
    else
      return false
    end
  end

  def get_ground_snow_load(elevation)
    if self.conditional_ground_snow_loads.length > 0
      conditionals = self.conditional_ground_snow_loads.sort_by &:elevation
      conditionals.each do |conditional|
        if conditional.comparison == "less than"
          if elevation < conditional.elevation
            return conditional.ground_snow_load
          end
        else
          if elevation >= conditional.elevation
            return conditional.ground_snow_load
          end
        end
      end

    else
      return self.ground_snow_load_pressure
    end
  end
end
