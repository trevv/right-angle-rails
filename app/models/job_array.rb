class JobArray < ApplicationRecord
  belongs_to :job
  has_many_attached :beam_size_images
  has_many_attached :beam_spacing_images
  has_many_attached :attic_images
  # after_save :run_calculation

  def self.assign_to_job(incoming, job_id)    
    if incoming[:name].blank?
      return
    end
    job_array = JobArray.new(
      name: incoming[:name],
      panel_quantity: incoming[:panel_quantity],
      ballast_weight_psf: incoming[:ballast_weight_psf],
      beam_span: incoming[:beam_span],
      beam_spacing: incoming[:beam_spacing],
      number_of_panels_on_rafter: incoming[:number_of_panels_on_rafter],
      panel_distance_from_eave: incoming[:panel_distance_from_eave],
      roof_pitch: incoming[:roof_pitch],
      orientation: incoming[:orientation],
      framing_type: incoming[:framing_type],
      framing_type_other: incoming[:framing_type_other],
      video_link: incoming[:video_link],
      job_id: job_id
    )
    job_array.save!
    job_array.attic_images.attach(incoming[:attic_images]) unless incoming[:attic_images].nil?
    job_array.beam_spacing_images.attach(incoming[:beam_spacing_images]) unless incoming[:beam_spacing_images].nil?
    job_array.beam_size_images.attach(incoming[:beam_size_images]) unless incoming[:beam_size_images].nil?

  end

  def run_calculation
    Calculation.run_calculations(self.job)
  end

end
