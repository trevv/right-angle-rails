class Company < ApplicationRecord
  serialize :stamp_pages, Array
  has_many :users
  has_many :jobs
  belongs_to :letter_type, optional: true
  # belongs_to :city
end
