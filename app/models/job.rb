class Job < ApplicationRecord
  has_one :estimate
  has_many :job_arrays

  has_many_attached :documents # letters
  has_many_attached :images # submitted images
  has_many_attached :layouts # submitted plans/layouts
  has_many_attached :roof_material_images # submitted roof material images
  belongs_to :connection_type
  belongs_to :user, optional: true
  belongs_to :company
  belongs_to :city, optional: true
  belongs_to :letter_type, optional: true

  accepts_nested_attributes_for :job_arrays

  validates :email,
            :solar_company_name,
            :client_last_name,
            :street_address,
            :city_name,
            :state,
            :zip,
            :home_elevation,
            :number_of_arrays,
            # :roof_pitch,
            presence: true

  attr_accessor :google_address_input, :street_number, :route, :administrative_area_level_1, :postal_code, :locality
  before_save :assign_priority
  before_save :match_city_name

  after_create do
    if city_is_serviced
      send_new_job_email
    else
      send_no_city_email
    end
  end

  def assign_priority
    case self.status
    when "In Review"
      self.priority = 0
    when "Approved"
      self.priority = 1
    when "Rejected"
      self.priority = 2
    when "Approved & Sent"
      self.priority = 3
    when "Rejected & Sent"
      self.priority = 4

    else
      self.priority = 0
    end
  end

  def match_city_name
    self.city_name = self.city.name
  end

  # CALLBACKS
  # before_validation do
  #   set_city

  #   if self.city
  #     set_city_fields
  #     # self.extend "Calculations::States::#{self.city.state}".constantize # this will no longer be necessary because the calculation variables will be stored in the city record
  #   else
  #     send_no_city_email
  #   end
  # end

  # after_validation do
  #   if self.city
  #     # set_static_fields
  #     # set_calculated_fields
  #   end
  # end

  # after_update do
  #   send_job_pdf_email if send_pdf == true
  # end



  # # INSTANCE METHODS
  # def set_city
  #   city_results = City.detect_by_name(city_name, state)
  #   self.city_id = city_results.first.id if city_results
  # end

  # def set_city_fields
  #   puts "Here's where we start doing calculations!"
  #   # self.city_elevation = city.elevation
  #   # self.ground_snow_load_pressure = city.ground_snow_load_pressure
  #   # self.design_wind_speed = city.design_wind_speed
  # end

  # # CUSTOM VALIDATIONS
  # def city_state_exists
  #   return if city_name.blank?

  #   if city_id.blank?
  #     errors.add(city_name, "is a city we don't do business in currently, but we hope to soon! We'll reach our shortly!")
  #   end
  # end

  # def job_has_all_fields
  #   attributes.select {|attr| self[attr].nil? }.count.zero?
  # end

  # EMAILS
  def send_no_city_email
    JobMailer.no_city_email(self).deliver_later
  end

  # def send_job_pdf_email
    # return false unless job_has_all_fields
    # DocumentMailer.with(job: self).job_pdf_email.deliver_later
  # end

  def send_new_job_email
    JobMailer.new_job_email(self).deliver_later
  end


  ##### NEW STUFF ######



  def city_is_serviced
    return !self.city_id.blank?
  end

  # creation method
    # create a JOB in the database that has all the user form inputs calculated out correctly
    # send out emails for new job / no city support email & customer confirmation of submission
    # create/generate document in document.rb, by pulling in the newly created JOB record

end
