class EstimateArray < ApplicationRecord
  belongs_to :estimate


  after_save :update_min_connections



  def update_min_connections
    estimate = self.estimate
    estimate.min_connections = estimate.estimate_arrays.sum(&:min_connections)
    estimate.save!
  end

end
