class State < ApplicationRecord
  has_many :cities
  has_many :letter_types
  has_many :stamps
end
