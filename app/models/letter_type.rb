class LetterType < ApplicationRecord
  has_many :jobs
  has_many :companies
  has_many :cities

  include SearchCop
  search_scope :search do
    attributes :name
  end
end
