class Photo < ApplicationRecord
  belongs_to :job
  has_attached_file :image,
    :size => { :less_than => 10.megabytes },
    :styles => {
      small: '450x300>',
      large: '1500x1000>'
    },
    path: "/storage/secure_files/:class/:attachment/:id_partition/:style/:filename",
    url: "/photos/:style/:id"

  validates_attachment :image, presence: true,
    content_type: { content_type: ['image/jpeg', 'image/png'] },
    size: { in: 0..10.megabytes }
end
