class DashboardService
  #  set pages which you wanna remove from sidebar
  HIDDEN_PAGES = [].freeze

  def self.routes(admin)
    admin.resources.reject { |i| i.resource.in?(HIDDEN_PAGES) }
  end
end