class Sheets
  include ApplicationHelper
  # Responsible for getting city/state information from Google Sheet and uploading to database
  require "google_drive"

  # Creates a session. This will prompt the credential via command line for the
  # first time and save it to config.json file for later usages.
  # See this document to learn how to create config.json:
  # https://github.com/gimite/google-drive-ruby/blob/master/doc/authorization.md
  @@session = GoogleDrive::Session.from_service_account_key("config/Right Angle-6a407ebc66e2.json")
  @@state_abbrev_hash = ApplicationController.helpers.states_hash

  # @@ws = @@session.spreadsheet_by_key("1kywpNQbii7046XXIw2rW5aVKgMoQwxbdiqPNKCKxY5s").worksheets[0]
  @@ws = @@session.spreadsheet_by_key("1kywpNQbii7046XXIw2rW5aVKgMoQwxbdiqPNKCKxY5s")

  def self.get_first_cell
    puts @@ws[2,1]
  end

  def self.update_the_database
    # @@ws = array of worksheets in the google shee

    # set up an empty array here - cities to upload
    cities_to_update = []
    conditionals_to_update = []
    # iterate through each worksheet
    @@ws.worksheets.each do |sheet|
    # Check to see if the State exists by the worksheet name
      state_name = sheet.title
      state_obj = State.find_by(name: state_name)

      # if not, create a state record and return the ID to assign to the incoming cities
      if !state_obj
        state_obj = State.create!(name: state_name, abbrev: @@state_abbrev_hash[state_name.to_sym])
      end
      # get the state ID to assign to the incoming cities
      state_id = state_obj.id

      columns = sheet.num_cols
      # the 1st row is headers, so use those to set the key
      headers = []
      columns.times do |x|
        # this gets the headers
        headers << sheet.rows[0][x]
        break if sheet.rows[0][x] == "notes"
        # we don't want anything extra, like conditionals - so we stop at the NOTES column
      end

      (1..sheet.num_rows).each do |row|
        city = {}
        saved = false
        (columns).times do |col|
          if col > headers.length
            # there must be data somewhere in this row that is a condition
            the_condition = {}
            # if blank, skip


            # first col = the the_condition, second = the ground snow load, account for it.
            next if !sheet[row, (col + 1)].include? "elevation"

            if sheet[row, (col + 1)].include? ">"
              the_condition["comparison"] = "greater than"
            else
              the_condition["comparison"] = "less than"
            end
            the_condition["elevation"] = sheet[row, (col + 1)].gsub(/[^0-9]/, '')
            the_condition["ground_snow_load"] = sheet[row, (col + 2)]
            the_condition["city_id"] = @@the_city.id

            conditional_obj = ConditionalGroundSnowLoad.find_or_create_by!(the_condition)
          end

          # populate the city object with key/val pairs equal to what's expected in the DB.
          city["#{headers[col.to_i]}"] = sheet[row, (col + 1)]

          if headers[col] == "notes"
            city["state_id"] = state_id
            @@the_city = City.find_or_initialize_by(name: city["name"], state_id: city["state_id"])

            # set attributes to be most recent from Google Sheet
            if @@the_city.name != city["name"] ||
              @@the_city.state_id != city["state_id"] ||
              @@the_city.ground_snow_load_pressure != city["ground_snow_load_pressure"] ||
              @@the_city.ibc != city["ibc"] ||
              @@the_city.iebc != city["iebc"] ||
              @@the_city.asce != city["asce"] ||
              @@the_city.notes != city["notes"]

              @@the_city.update(city)
              begin
                @@the_city.save!
              rescue => exception
                next
              end
            end
            # remove all conditionals, prior to setting them again.
            ConditionalGroundSnowLoad.where(city_id: @@the_city.id).destroy_all

          end
        end
      end
    end
    # do a bulk upsert operation, on duplicate key (which should be city & state_id) update everything


  end




  def states_hash
    return states_hash =
    {
      'Alabama': 'AL',
      'Alaska': 'AK',
      'American Samoa': 'AS',
      'Arizona': 'AZ',
      'Arkansas': 'AR',
      'California': 'CA',
      'Colorado': 'CO',
      'Connecticut': 'CT',
      'Delaware': 'DE',
      'District Of Columbia': 'DC',
      'Federated States Of Micronesia': 'FM',
      'Florida': 'FL',
      'Georgia': 'GA',
      'Guam': 'GU',
      'Hawaii': 'HI',
      'Idaho': 'ID',
      'Illinois': 'IL',
      'Indiana': 'IN',
      'Iowa': 'IA',
      'Kansas': 'KS',
      'Kentucky': 'KY',
      'Louisiana': 'LA',
      'Maine': 'ME',
      'Marshall Islands': 'MH',
      'Maryland': 'MD',
      'Massachusetts': 'MA',
      'Michigan': 'MI',
      'Minnesota': 'MN',
      'Mississippi': 'MS',
      'Missouri': 'MO',
      'Montana': 'MT',
      'Nebraska': 'NE',
      'Nevada': 'NV',
      'New Hampshire': 'NH',
      'New Jersey': 'NJ',
      'New Mexico': 'NM',
      'New York': 'NY',
      'North Carolina': 'NC',
      'North Dakota': 'ND',
      'Northern Mariana Islands': 'MP',
      'Ohio': 'OH',
      'Oklahoma': 'OK',
      'Oregon': 'OR',
      'Palau': 'PW',
      'Pennsylvania': 'PA',
      'Puerto Rico': 'PR',
      'Rhode Island': 'RI',
      'South Carolina': 'SC',
      'South Dakota': 'SD',
      'Tennessee': 'TN',
      'Texas': 'TX',
      'Utah': 'UT',
      'Vermont': 'VT',
      'Virgin Islands': 'VI',
      'Virginia': 'VA',
      'Washington': 'WA',
      'West Virginia': 'WV',
      'Wisconsin': 'WI',
      'Wyoming': 'WY'
    }
  end
end
