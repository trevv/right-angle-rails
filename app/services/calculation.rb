class Calculation
  # Responsible for getting city/state/job specific variables and correctly calculating values

  def self.run_calculations(job)
    @@job = job

    # @@calc."whatever" in the lines below reference the functions from lines 103 and down.
    # Example: @@calc.general_roof_pitch (seen on line 21) refers to function on line 108
    @@calc = Calculation.new
    @@ground_snow_load = @@job.city.get_ground_snow_load(@@job.home_elevation || @@job.city.elevation)
    @@structure_type = @@job.structure_type
    @@enclosure = @@job.enclosure.downcase
    @@wind_load = @@job.city.wind_load
    @@velocity_pressure = @@calc.velocity_pressure
    @@number_of_arrays = @@job.number_of_arrays
    @@framing_type = @@job.roof_framing_type
    @@connection_type = @@job.connection_type
    @@roof_material = @@job.roof_material
    @@roof_material_psf = @@calc.roof_material_psf
    # assign minimum roof pitch with following method
    @@calc.general_roof_pitch
    @@solar_panels_arrangement = @@calc.solar_panels_arrangement
    @@solar_panels_psf = @@calc.solar_panels_psf
    @@dl_without_panels = @@calc.dl_without_panels #()
    @@dl_with_panels = @@calc.dl_with_panels #()
    @@flat_roofs_1 = @@calc.flat_roofs_1 #()
    @@slippery_surface = @@calc.slippery_surface #()
    @@non_slippery_surface = @@calc.non_slippery_surface #()
    @@sl_without_panels = @@calc.sl_without_panels #()
    @@sl_with_panels = @@calc.sl_with_panels #()
    @@dllrwith = @@calc.dl_with_panels #() ??? REALLY?
    @@dllrwithout = @@calc.dllrwithout #() ??? REALLY?
    @@dlslwithout = @@calc.dlslwithout #()
    @@dlslwith = @@calc.dlslwith #()

    @@job_arrays = @@job.job_arrays.length

    # puts "Executing setting of variables...\n
    # Ground Snow Load: #{@@ground_snow_load}\n
    # Structure type: #{@@structure_type}\n
    # Enclosure: #{@@enclosure}\n
    # Wind load: #{@@wind_load}\n
    # Velocity pressure: #{@@velocity_pressure}\n
    # Roof material: #{@@roof_material}\n
    # Number of Arrays: #{@@number_of_arrays}\n
    # Minimum Roof pitch: #{@@min_roof_pitch}\n
    # Framing type: #{@@framing_type}\n
    # Connection type: #{@@connection_type}\n
    # Roof material: #{@@roof_material}\n
    # Roof material PSF: #{@@roof_material_psf}\n
    # Solar Panels PSF (need to finish): #{@@solar_panels_psf}\n
    # DL without panels: #{@@dl_without_panels}\n
    # DL with panels: #{@@dl_with_panels}\n
    # Slippery surface: #{@@slippery_surface}\n
    # Non-slippery surface: #{@@non_slippery_surface}\n
    # SL with panels (need to finish): #{@@sl_with_panels}\n

    # SL without panels: #{@@sl_without_panels}\nDLLR with: #{@@dllrwith}\n
    # DLLR without: #{@@dllrwithout}\n
    # DLSL with: #{@@dlslwith}\n
    # DLSL without: #{@@dlslwithout}\n
    # Flat roofs1: #{@@flat_roofs_1}\n
    # # of Job Arrays: #{@@job_arrays}
    # "
    @@estimate = Estimate.where(job_id: @@job.id).first_or_create do |est|
      est.job_id =  @@job.id
      est.company_id =  @@job.company_id
      est.risk_category =  2
      est.wind_exposure_category =  "C"
      est.wind_load =  @@wind_load
      est.building_type =  @@enclosure
      est.roof_material =  @@roof_material
      est.roof_material_psf =  @@roof_material_psf
      est.solar_panel_arrangement =  @@solar_panels_arrangement
      est.solar_panel_psf =  @@solar_panels_psf
      est.design_wind_speed =  @@wind_load
      est.ground_sl =  @@ground_snow_load
      est.flat_roof_sl =  @@flat_roofs_1
      est.slippery_surface =  @@slippery_surface
      est.non_slippery_surface =  @@non_slippery_surface
      est.sl_without_panels =  @@sl_without_panels
      est.sl_with_panels =  @@sl_with_panels
      est.dl_without_panels =  @@dl_without_panels
      est.dl_with_panels =  @@dl_with_panels
      est.dlsl_without_panels =  @@dlslwithout
      est.dlsl_with_panels =  @@dlslwith
      est.dllr_without_panels =  @@dllrwithout
      est.dllr_with_panels =  @@dllrwith
    end

    @@estimate.save!

    @@job.job_arrays.each do |job_array|
      @@calc.execute_job_array_estimate(job_array)
    end



  end




  ################################## GENERAL JOB STUFF ##############################



  def general_roof_pitch
    @@min_roof_pitch_job_array = @@job.job_arrays.min_by(&:roof_pitch)
    @@min_roof_pitch = @@min_roof_pitch_job_array.roof_pitch
    return
  end

#ROOF DEAD LOAD - ASCE TABLE C3-1
  def roof_material_psf
    case @@roof_material.downcase
      when "asphalt shingles" then return 2
      when "corrugated metal" then return 3
      when "concrete tiles" then return 12
      when "clay tiles" then return 12
      when "TPO membrane" then return 1.5
      when "gravel" then return 6
      when "spanish tiles" then return 19
    end
  end

  def solar_panels_arrangement
    return !@@min_roof_pitch_job_array.ballast ? "Solar Panel Array" : "Solar Panel Array and Ballast"
  end

  def solar_panels_psf
    return !@@min_roof_pitch_job_array.ballast ? 3 : @@min_roof_pitch_job_array.ballast_weight_psf
  end

  def dl_without_panels
    return @@roof_material_psf + 1.5 + 4 + 3.85 + 2
  end

  # get values from solar_panels_psf ???
  def dl_with_panels
    return @@roof_material_psf + 1.5 + 4 + 3.85 + 2 + @@solar_panels_psf
  end

#ROOF LIVE LOAD - State Specific Stuff that determines 1bc1, iebc, ibc2, iebc2, and fivepercent = 5

#ROOF SNOW LOAD ASCE7-10

  def flat_roofs_1
    return 0.7 * 0.9 * 1.1 * 1 * @@ground_snow_load
  end

  def slippery_surface
    if @@min_roof_pitch > 70
      return 0
    elsif @@min_roof_pitch > 10
      return (1 - (@@min_roof_pitch - 10) / 60.0)
    else
      return 1
    end
  end

  def non_slippery_surface
    if @@min_roof_pitch > 70
      return 0
    elsif @@min_roof_pitch > 37.5
      return (1 - (@@min_roof_pitch - 37.5) / 32.5)
    else
      return 1
    end
  end

  #WHERE DOES SL_WITH_PANELS COME FROM?
  def sl_with_panels
    if ["Elko", "Boise"].include? @@job.city.name
      return @@sl_without_panels
    else
      return @@slippery_surface * @@flat_roofs_1
    end
  end

  def sl_without_panels
    return @@non_slippery_surface * @@calc.flat_roofs_1
  end

  # CITY SPECIFIC - SOME WANT TO MEASURE SL_WITH_PANELS AS THE SAME AS WITHOUT

#LOAD COMBINATIONS
  def dllrwithout
    return @@dl_without_panels + 20
  end

  def dllrwith
    return @@dl_with_panels
  end

  def dlslwithout
    return @@dl_without_panels + @@sl_without_panels
  end

  def dlslwith
    return @@dl_with_panels + @@sl_with_panels
  end


  ######################################### THESE DETERMINE ARRAY STUFF ################

  # get roofpitch1 from general_roof_pitch above

  def panel_area
    return @@panel_total.to_f * 17.5
  end

#WIND CALCS - ASCE 7-10
  def zone1
    if  @@roof_pitch <= 7
      return -1
    elsif @@roof_pitch < 27
      return -0.9
    elsif @@roof_pitch >= 27
      return -1
    end
  end

  def zone2
    if @@roof_pitch <= 7
      return -1.8
    elsif @@roof_pitch < 27
      return -1.7
    elsif @@roof_pitch >= 27
      return -1.2
    end
  end

  def zone3
    if @@roof_pitch <= 7
      return -2.8
    elsif @@roof_pitch < 27
      return -2.6
    elsif @@roof_pitch >= 27
      return -1.2
    end
  end

  def gcpi
    case @@enclosure
    when "open" then @@gcpi = 0
    when "partially enclosed" then @@gcpi = 0.55
    when "enclosed" then @@gcpi = 0.18
    end
  end

  def velocity_pressure
    return 0.00256 * 0.98 * 1 * 0.85 * @@wind_load ** 2
  end

  def zone1_pressure
    return @@velocity_pressure * (@@zone1 - @@gcpi)
  end

  def zone2_pressure
    return @@velocity_pressure * (@@zone2 - @@gcpi)
  end

  def zone3_pressure
    return @@velocity_pressure * (@@zone3 - @@gcpi)
  end

#LAG SCREW CONNECTION
  def sheartribarea
    return @@shearcapacity / ((@@sl_with_panels + @@solar_panels_psf) * Math.sin(@@roof_pitch * 3.14159265 / 180))
  end

  def pullouttribarea
    return (@@pulloutcapacity / @@zone2_pressure).abs
  end

  def connections
    cos_equation = (@@panel_area * Math.cos(@@roof_pitch * (3.1415926 / 180)) / @@pullouttribarea)
    sin_equation = (@@panel_area * Math.sin(@@roof_pitch * (3.1415926 / 180)) / @@sheartribarea)
    return cos_equation > sin_equation ? cos_equation : sin_equation
  end

  def fs
    if (@@connections * 1.2) < (2 * @@panel_total)
      return (2 * @@panel_total) / @@connections
    else
      return 1.2
    end
  end

  def connection_fs
    return @@connections * @@fs
  end

#ROOF CONNECTION

def get_ndsshear
  return @@connection_type.nds_shear
end

def get_ndspullout
  return @@connection_type.nds_pullout
end

def get_shearcapacity
  return @@connection_type.shear
end

def get_pulloutcapacity
  return @@connection_type.pullout
end

def get_pullouttribarea
  return (@@pulloutcapacity / @@zone2_pressure).abs
end


def execute_job_array_estimate(job_array)
  @@job_array = job_array
  @@panel_total = @@job_array.panel_quantity # defined in @@JOB_ARRAY
  @@panel_area = @@calc.panel_area
  @@v2percentage = 0 # init
  @@v1percentage = 0 # init
  @@connection_fs = 0 #init
  @@bending_moment_percentage = 0 # init
  @@ndsshear = @@calc.get_ndsshear
  @@ndspullout = @@calc.get_ndspullout
  @@fs = 1.1
  @@gcpi = @@calc.gcpi # from city

  # OTHER THINGS HERE THAT WON'T BE AFFECTED BY INCREMENTING FS
  @@beamspan = @@job_array.beam_span #defined in JobArray
  @@spacing = @@job_array.beam_spacing/12 #defined in JobArray ... beam_spacing is in inches. dividing by 12 so that it's calculated in feet. 6/22/19
  @@panel_on_rafter = @@job_array.number_of_panels_on_rafter #defined in JobArray
  @@panel_distance = @@job_array.panel_distance_from_eave #defined in JobArray
  @@roof_framing_type = @@calc.roof_framing #defined in JobArray
  @@roof_pitch = @@job_array.roof_pitch #defined in JobArray
  @@orientation = @@job_array.orientation #defined in JobArray
  @@zone1 = @@calc.zone1
  @@zone2 = @@calc.zone2
  @@zone3 = @@calc.zone3
  @@times = 1
  # @@sl_without_panels = @@sl_without_panels # defined in the job above

  until @@connection_fs > (@@panel_total * 2) && @@v2percentage < 105 && @@v1percentage < 105 && @@bending_moment_percentage < 105 || @@times > 100 do
    @@times += 1
    @@fs += 0.1
    @@shearcapacity = @@calc.get_shearcapacity #()
    @@pulloutcapacity = @@calc.get_pulloutcapacity #()
    @@sheartribarea = @@calc.sheartribarea #()
    @@zone1_pressure = @@calc.zone1_pressure
    @@zone2_pressure = @@calc.zone2_pressure
    @@zone3_pressure = @@calc.zone3_pressure
    @@pullouttribarea = @@calc.get_pullouttribarea #()
    @@connections = @@calc.connections #()
    @@connection_fs = @@calc.connection_fs #()
    @@snow_solar = @@calc.snow_solar #()
    @@rack_spacing = @@calc.rack_spacing #()
    @@sllocation = @@calc.sllocation #()
    @@slshear = @@calc.slshear #()

    @@v2_without = @@calc.v2_without #()
    @@v1_without = @@calc.v1_without #()

    @@v2_with = @@calc.v2_with #()
    @@v1_with = @@calc.v1_with #()
    @@v2percentage = @@calc.v2percent
    @@v1percentage = @@calc.v1percent

    # NEED TO DEAL WITH TRIANGLES/SQUARE STUFF HERE
    @@point2 = @@calc.point_2
    @@point3 = @@calc.get_odd_point(@@point2)
    @@point4 = @@calc.get_even_point(@@point3)
    @@point5 = @@calc.get_odd_point(@@point4)
    @@point6 = @@calc.get_even_point(@@point5)
    @@point7 = @@calc.get_odd_point(@@point6)
    @@point8 = @@calc.get_even_point(@@point7)
    @@point9 = @@calc.get_odd_point(@@point8)
    @@point10 = @@calc.get_even_point(@@point9)
    @@point11 = @@calc.get_odd_point(@@point10)
    @@point12 = @@calc.get_even_point(@@point11)
    @@point13 = @@calc.get_odd_point(@@point12)
    @@point14 = @@calc.get_even_point(@@point13)
    @@point15 = @@calc.get_odd_point(@@point14)
    @@triangle1 = @@calc.triangle_1
    @@triangle2 = @@calc.get_triangle(@@point3, @@point4)
    @@triangle3 = @@calc.get_triangle(@@point5, @@point6)
    @@triangle4 = @@calc.get_triangle(@@point7, @@point8)
    @@triangle5 = @@calc.get_triangle(@@point9, @@point10)
    @@triangle6 = @@calc.get_triangle(@@point11, @@point12)
    @@triangle7 = @@calc.get_triangle(@@point13, @@point14)
    @@square1 = @@calc.get_square1(@@point2)
    @@square2 = @@calc.get_square(@@point4)
    @@square3 = @@calc.get_square(@@point6)
    @@square4 = @@calc.get_square(@@point8)
    @@square5 = @@calc.get_square(@@point10)
    @@square6 = @@calc.get_square(@@point12)
    @@square7 = @@calc.get_square(@@point14)
    @@triangle_axis2 = @@calc.get_triangle_axis(@@point3)
    @@triangle_axis3 = @@calc.get_triangle_axis(@@point5)
    @@triangle_axis4 = @@calc.get_triangle_axis(@@point7)
    @@triangle_axis5 = @@calc.get_triangle_axis(@@point9)
    @@triangle_axis6 = @@calc.get_triangle_axis(@@point11)
    @@triangle_axis7 = @@calc.get_triangle_axis(@@point13)
    @@bending_moment_without = @@calc.bending_moment_without #()
    @@bending_moment_with = @@panel_on_rafter == 1 ? @@calc.get_bending_moment_with_panel_on_rafter1 : @@calc.bending_moment_with #()
    @@bending_moment_percentage = @@calc.bending_moment_percent
    @@bending_moment_105 = @@calc.bending_moment_105
    @@v1_105 = @@calc.v1_105
    @@v2_105 = @@calc.v2_105


    puts "Executing setting of variables for array...\n
    v1with: #{@@v1_with}\n
    v1without: #{@@v1_without}\n
    v2with: #{@@v2_with}\n
    v2without: #{@@v2_without}\n
    v2_percentage: #{@@v2percentage}\n
    v1_percentage: #{@@v1percentage}\n
    Connection FSs: #{@@connection_fs}\n
    fs: #{@@fs}\n
    Bending Moment Without: #{@@bending_moment_without}\n
    Bending moment with: #{@@bending_moment_with}\n
    Bending moment %: #{@@bending_moment_percentage}\n
    Bending moment 105: #{@@bending_moment_105}\n
    met qualifications to exit loop? #{@@connection_fs > (@@panel_total * 2) && @@v2percentage < 105 && @@v1percentage < 105 && @@bending_moment_percentage < 105 }

    triangle_axis 2: #{@@triangle_axis2}\n
    triangle_axis 3: #{@@triangle_axis3}\n
    triangle_axis 4: #{@@triangle_axis4}\n
    triangle_axis 5: #{@@triangle_axis5}\n
    triangle_axis 6: #{@@triangle_axis6}\n
    triangle_axis 7: #{@@triangle_axis7}\n
    square 1: #{@@square1}\n
    square 2: #{@@square2}\n
    square 3: #{@@square3}\n
    square 4: #{@@square4}\n
    square 5: #{@@square5}\n
    square 6: #{@@square6}\n
    square 7: #{@@square7}\n
    point2: #{@@point2}\n
    point3: #{@@point3}\n
    point4: #{@@point4}\n
    point5: #{@@point5}\n
    point6: #{@@point6}\n
    point7: #{@@point7}\n
    point8: #{@@point8}\n
    point9: #{@@point9}\n
    point10: #{@@point10}\n
    point11: #{@@point11}\n
    point12: #{@@point12}\n
    point13: #{@@point13}\n
    point14: #{@@point14}\n
    point15: #{@@point15}\n
    Triangle 1: #{@@triangle1}\n
    Triangle 2: #{@@triangle2}\n
    Triangle 3: #{@@triangle3}\n
    Triangle 4: #{@@triangle4}\n
    Triangle 5: #{@@triangle5}\n
    Triangle 6: #{@@triangle6}\n
    Triangle 7: #{@@triangle7}\n"


  end

  # SAVE THE JOB ARRAY ESTIMATE TO THE @@ESTIMATE
  job_array_estimate = EstimateArray.where(name: @@job_array.name, estimate_id: @@estimate.id).first_or_create do |est_array|
    est_array.estimate_id = @@estimate.id
    est_array.name = @@job_array.name
    est_array.number_of_panels = @@panel_total
    est_array.slope = @@roof_pitch
    est_array.panel_area = @@panel_area
    est_array.zone1 = @@zone1
    est_array.zone2 = @@zone2
    est_array.zone3 = @@zone3
    est_array.zone1_pressure = @@zone1_pressure
    est_array.zone2_pressure = @@zone2_pressure
    est_array.zone3_pressure = @@zone3_pressure
    est_array.velocity_pressure = @@velocity_pressure
    est_array.gcpi = @@gcpi
    est_array.shear_capacity = @@shearcapacity
    est_array.shear_trib_area = @@sheartribarea
    est_array.pullout_capacity = @@pulloutcapacity
    est_array.pullout_trib_area = @@pullouttribarea
    est_array.nds_shear = @@ndsshear
    est_array.nds_pullout = @@ndspullout
    est_array.fs = @@fs
    est_array.min_connections = @@connection_fs
    est_array.beam_span = @@beamspan
    est_array.spacing = @@spacing
    est_array.panels_per_rafter = @@panel_on_rafter
    est_array.panel_distance_from_eave = @@panel_distance
    est_array.roof_framing_type = @@roof_framing_type
    est_array.orientation = @@orientation
    est_array.bending_moment_without = @@bending_moment_without
    est_array.bending_moment_with = @@bending_moment_with
    est_array.bending_moment_percentage = @@bending_moment_percentage
    est_array.bending_moment_105 = @@bending_moment_105
    est_array.v1_with = @@v1_with
    est_array.v2_with = @@v2_with
    est_array.v1_without = @@v1_without
    est_array.v2_without = @@v2_without
    est_array.v1_percentage = @@v1percentage
    est_array.v2_percentage = @@v2percentage
    est_array.v1_105 = @@v1_105
    est_array.v2_105 = @@v2_105
  end
  job_array_estimate.save!


end


#BEAM STRESS IEBC - ALL DEFINED IN JOBARRAY

  # roof framing - OTHER checkbox checked? Otherwise selection from dropdown
  def roof_framing
    return !@@job_array.framing_type_is_other ? @@job_array.framing_type : @@job_array.framing_type_other
  end

  def snow_solar()
    return ((@@sl_with_panels * @@panel_area) / @@connection_fs) + ((@@solar_panels_psf * @@panel_area) / @@connection_fs)
  end

  def rack_spacing
    return @@orientation.downcase == "portrait" ? 5.416667 : 3.25
  end

  def v1_without
    return @@dlslwithout > @@dllrwithout ? (@@dlslwithout * @@beamspan * @@spacing) / 2 : (@@dllrwithout * @@beamspan * @@spacing) / 2
  end

  def v2_without #()
    return @@dlslwithout > @@dllrwithout ? (@@dlslwithout * @@beamspan * @@spacing) / 2 : (@@dllrwithout * @@beamspan * @@spacing) / 2
  end

  def bending_moment_without
    return @@dlslwithout > @@dllrwithout ? (@@dlslwithout * @@spacing * (@@beamspan**2)) / 8 : (@@dllrwithout * @@spacing * (@@beamspan**2)) / 8
  end

  def sllocation() # TODO check on then statement
    return @@panel_on_rafter == 1  ? ((@@beamspan / 2) - (@@rack_spacing / 2)) / 2 : 0
  end

  def slshear() # TODO check on then statement
    return @@panel_on_rafter == 1 ? (@@sl_without_panels * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2))) : 0
  end

  def v2_with
    num = @@panel_on_rafter
    if num == 1
      return ((@@slshear * @@sllocation) + ((@@dl_without_panels * @@beamspan * @@spacing) * (@@beamspan / 2)) + (@@snow_solar * (@@beamspan / 2)) + (@@slshear * (@@beamspan - @@sllocation))) / @@beamspan
    else
      num = num - 1
      total = 0
      num.times do |x|
        x = x + 1
        total += ((@@snow_solar * (@@panel_distance + 1 + (@@rack_spacing * x))))
      end
      return ((((@@sl_without_panels * @@panel_distance * @@spacing) * (@@panel_distance / 2)) + (@@snow_solar * (@@panel_distance + 1))   + total                                           + ((@@dl_without_panels * @@beamspan * @@spacing) * (@@beamspan / 2) + ((@@sl_without_panels * @@spacing * (@@beamspan - ((@@panel_on_rafter * @@rack_spacing) + @@panel_distance))) * (((@@beamspan - (@@panel_distance + (@@rack_spacing * @@panel_on_rafter))) / 2) + (@@panel_distance + (@@rack_spacing * @@panel_on_rafter)))))) / @@beamspan)
    end

  end

  def v1_with
    num = @@panel_on_rafter
    if num == 1
      return ((@@slshear * @@sllocation) + ((@@dl_without_panels * @@beamspan * @@spacing) * (@@beamspan / 2)) + (@@snow_solar * (@@beamspan / 2)) + (@@slshear * (@@beamspan - @@sllocation))) / @@beamspan
    else
      num = num - 1
      return @@sl_without_panels * @@panel_distance * @@spacing + (@@snow_solar * num) + (@@dl_without_panels * @@beamspan * @@spacing) + @@snow_solar - @@v2_with + (@@sl_without_panels * @@spacing * (@@beamspan - ((@@panel_on_rafter * @@rack_spacing) + @@panel_distance)))
    end
  end

  # to get point 2 / START
  def point_2
    return @@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * (@@panel_distance + 1))
  end

  # to get odd points
  def get_odd_point(point)
    return point - @@snow_solar
  end

  # to get even points
  def get_even_point(point)
    return point - (@@dl_without_panels * @@spacing * @@rack_spacing)
  end

  # triangle 1
  def triangle_1
    return (((@@v1_with - @@point2) * (@@panel_distance + 1)) / 2)
  end

  # triangles 2-7
  def get_triangle(first_point, second_point)
    return (((first_point - second_point) * @@rack_spacing) / 2)
  end

  # square 1
  def get_square1(point)
    return point * @@panel_distance
  end

  # squares 2-7
  def get_square(point)
    return point * @@rack_spacing
  end

  # get triangle axis for 2 - 7
  def get_triangle_axis(point)
    return ((point / (@@dl_without_panels * @@spacing)) * point) / 2
  end

  def get_bending_moment_with_panel_on_rafter1
    return ((@@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2)))) * ((@@beamspan / 2) - (@@rack_spacing / 2)) + ((@@v1_with - (@@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2))))) * ((@@beamspan / 2) - (@@rack_spacing / 2))) / 2) + (((@@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2)))) - (@@dl_without_panels * @@spacing * (@@rack_spacing / 2))) * (@@rack_spacing / 2)) + ((((@@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2)))) - ((@@v1_with - ((@@dl_without_panels + @@sl_without_panels) * @@spacing * ((@@beamspan / 2) - (@@rack_spacing / 2)))) - (@@dl_without_panels * @@spacing * (@@rack_spacing / 2)))) * (@@rack_spacing / 2)) / 2)
  end

  def bending_moment_with

    if @@point3 < 0
      @@bendingmomentwith = @@triangle1 + ( @@point2 * @@panel_distance)
    elsif @@point4 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle_axis2
    elsif @@point5 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + (@@point4 * @@rack_spacing)
    elsif @@point6 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle_axis3
    elsif @@point7 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + (@@point6 * @@rack_spacing)
    elsif @@point8 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle_axis4
    elsif @@point9 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + (@@point8 * @@rack_spacing)
    elsif @@point10 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + @@square4 + @@triangle_axis5
    elsif @@point11 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + @@square4 + @@triangle5 + (@@point10 * @@rack_spacing)
    elsif @@point12 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + @@square4 + @@triangle5 + @@square5 + @@triangle_axis6
    elsif @@point13 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + @@square4 + @@triangle5 + @@square5 + @@triangle6 + (@@point12 * @@rack_spacing)
    elsif @@point14 < 0
      @@bendingmomentwith = @@triangle1 + @@square1 + @@triangle2 + @@square2 + @@triangle3 + @@square3 + @@triangle4 + @@square4 + @@triangle5 + @@square5 + @@triangle6 + @@square6 + @@triangle_axis7
    end
    # NONE OF THOSE IF STATEMENTS ARE SATISFIED... SO WHAT SHOULD @@bendingmomentwith default to?

  end

  def v2percent
    return (@@v2_with / @@v2_without) * 100
  end

  def v1percent
    return (@@v1_with / @@v1_without) * 100
  end

  def v2_105
    return @@v2percentage < 105 ? "Less than 105%" : "Fail"
  end

  def v1_105
    return @@v1percentage < 105 ? "Less than 105%" : "Fail"
  end

  def bending_moment_percent
    return (@@bending_moment_with / @@bending_moment_without) * 100
  end

  def bending_moment_105
    return @@bending_moment_percentage < 105 ? "Less than 105%" : "Fail"
  end

end
