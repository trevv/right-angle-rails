require "administrate/base_dashboard"

class JobArrayDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    job: Field::BelongsTo,
    id: Field::Number,
    name: Field::String.with_options(searchable: true),
    panel_quantity: Field::Number,
    roof_pitch: Field::String.with_options(searchable: false),
    ballast: Field::Boolean,
    ballast_weight_psf: Field::String.with_options(searchable: false),
    beam_span: Field::String.with_options(searchable: false),
    beam_spacing: Field::String.with_options(searchable: false),
    framing_type: Field::String,
    framing_type_is_other: Field::Boolean,
    framing_type_other: Field::String,
    number_of_panels_on_rafter: Field::Number,
    panel_distance_from_eave: Field::String.with_options(searchable: false),
    orientation: Field::String,
    video_link: Field::String,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :job,
    :id,
    :name    
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :job,
    :id,
    :name,
    :panel_quantity,
    :roof_pitch,
    :ballast,
    :ballast_weight_psf,
    :beam_span,
    :beam_spacing,
    :framing_type,
    :framing_type_is_other,
    :framing_type_other,
    :number_of_panels_on_rafter,
    :panel_distance_from_eave,
    :orientation,
    :video_link,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :job,
    :name,
    :panel_quantity,
    :roof_pitch,
    :ballast,
    :ballast_weight_psf,
    :beam_span,
    :beam_spacing,
    :framing_type,
    :framing_type_is_other,
    :framing_type_other,
    :number_of_panels_on_rafter,
    :panel_distance_from_eave,
    :orientation,
    :video_link,
  ].freeze

  # Overwrite this method to customize how job arrays are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(job_array)
    "#{job_array.job.client_last_name} - Job Array #{job_array.id}"
  end
end
