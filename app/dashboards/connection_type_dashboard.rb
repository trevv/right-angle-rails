require "administrate/base_dashboard"

class ConnectionTypeDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    name: Field::String,
    shear: Field::Number.with_options(decimals: 2),
    pullout: Field::Number.with_options(decimals: 2),
    nds_shear: Field::String,
    nds_pullout: Field::String,
    active: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :name,
    :shear,
    :pullout,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :shear,
    :pullout,
    :nds_shear,
    :nds_pullout,
    :active,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :shear,
    :pullout,
    :nds_shear,
    :nds_pullout,
    :active,
  ].freeze

  # Overwrite this method to customize how connection types are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(connection_type)
    "#{connection_type.name} Connection Type"
  end
end
