require "administrate/base_dashboard"

class ConditionalGroundSnowLoadDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    city: Field::BelongsTo.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    id: Field::Number,
    comparison: Field::String,
    elevation: Field::Number,
    ground_snow_load: Field::Number.with_options(decimals: 2),
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :city,
    :comparison,
    :elevation,
    :ground_snow_load
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :city,
    :id,
    :comparison,
    :elevation,
    :ground_snow_load,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :city,
    :comparison,
    :elevation,
    :ground_snow_load,
  ].freeze

  # Overwrite this method to customize how conditional ground snow loads are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(conditional_ground_snow_load)
    "Conditional Ground Snow Load ##{conditional_ground_snow_load.id}"
  end
end
