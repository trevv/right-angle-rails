require "administrate/base_dashboard"

class StateDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    cities: Field::HasMany.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    id: Field::Number,
    name: Field::String.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    abbrev: Field::String.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    elevation: Field::Number,
    ground_snow_load_pressure: Field::Number,
    design_wind_speed: Field::Number,
    ibc: Field::Text,
    iebc: Field::Text,
    ibc2: Field::Text,
    iebc2: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :name,
    :abbrev,
    :cities,
    :id,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :name,
    :abbrev,

    :elevation,
    :ground_snow_load_pressure,
    :design_wind_speed,
    :ibc,
    :iebc,
    :ibc2,
    :iebc2,
    :created_at,
    :updated_at,
    :cities
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :name,
    :abbrev,
    :elevation,
    :ground_snow_load_pressure,
    :design_wind_speed,
    :ibc,
    :iebc,
    :ibc2,
    :iebc2,
    :cities
  ].freeze

  # Overwrite this method to customize how states are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(state)
    state.name
  end
end
