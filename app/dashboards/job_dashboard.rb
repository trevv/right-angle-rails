require "administrate/base_dashboard"

class JobDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    user: Field::BelongsTo.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    company: Field::BelongsTo.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    city: Field::BelongsTo.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    id: Field::Number,
    send_pdf: Field::Boolean,
    # is_premanufacture_truss: Field::Boolean,
    email: Field::String,
    solar_company_name: Field::String,
    client_last_name: Field::String,
    street_address: Field::String,
    city_name: Field::String.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    state: Field::String.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    status: Field::String.with_options(
      searchable: true,
      searchable_field: 'status'
    ),
    connection_type: Field::BelongsTo.with_options(
      searchable: true,
      searchable_field: 'name'
    ),
    number_of_arrays: Field::Number,
    zip: Field::Number,
    roof_square_footage: Field::String.with_options(searchable: false),
    home_elevation: Field::Number,
    notes: Field::Text,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [

    :company,
    :city,
    :connection_type,
    :created_at
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [

    # :images,
    :user,
    :company,
    :city,
    :id,
    :notes,
    # :send_pdf,
    # :is_premanufacture_truss,
    :email,
    :solar_company_name,
    :client_last_name,
    :street_address,
    :city_name,
    :state,
    :zip,
    :roof_square_footage,
    :home_elevation,
    :connection_type,
    # :number_of_panels,
    # :roof_pitch,
    # :risk_category,
    # :city_elevation,
    # :wind_exposure,
    # :design_wind_speed,
    # :ground_snow_load_pressure,
    # :roof_dead_load_pressure,
    # :solar_array_load_pressure,
    # :total_solar_array_area,
    # :roof_dead_load_pounds,
    # :roof_snow_load_pressure,
    # :roof_snow_load_pounds,
    # :solar_array_load_pounds,
    # :roof_stress_percent,
    # :wind_pressure,
    # :tributary_area_per_connection,
    # :min_embedment_lag_screw,
    # :lag_screw_pullout_capacity,
    # :lag_screw_shear_capacity,
    # :minimum_number_of_connections,
    :number_of_arrays,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [

    :company,
    :city,
    # :send_pdf,
    :notes,
    :email,
    :solar_company_name,
    :client_last_name,
    :street_address,
    :city_name,
    :state,
    :zip,
    :roof_square_footage,
    :home_elevation,
    :connection_type,
    :number_of_arrays
  ].freeze

  # Overwrite this method to customize how jobs are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(job)
    "#{job.client_last_name} Job"
  end
end
