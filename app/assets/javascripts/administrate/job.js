$("#mark-accepted").click(function () {
  var id = $('#mark-accepted').data('job-id');
  $.ajax({
    type: "PATCH",
    url: "/admin/job/" + id + "/mark_approved",
    async: true,
    success: function () {
      $("#job-status").text("Approved");
      $("#job-status").css("color", "green");
      $('#send-email').css("display", "flex");
    },
    error: function () {
      $("#job-status").text("Awaiting Review");
      alert("ERROR: Could not successfully approve this job. Try again.");
    }
  });
});

$("#mark-rejected").click(function () {
  var id = $('#mark-rejected').data('job-id');
  $.ajax({
    type: "PATCH",
    url: "/admin/job/" + id + "/mark_rejected",
    async: true,
    success: function () {
      $("#job-status").text("Rejected");
      $('#job-status').css("color", "red");
      $('#send-email').css("display", "flex");
    },
    error: function () {
      $("#job-status").text("Awaiting Review");
      alert("ERROR: Could not successfully approve this job. Try again.");
    }
  });
});

$("#send-email").click(function () {
  var id = $('#mark-accepted').data('job-id');
  $('.modal').css("display", "flex");
  $.ajax({
    type: "GET",
    url: "/admin/job/" + id + "/email_to_client",
    async: true,
    success: function () {
      var status = $("#job-status").text();
      $("#job-status").text(status + " & Sent");
      $('#job-status').css("color", "blue");
      $('#send-email').css("display", "flex");
      $("#send-email").prop("disabled", false);
    },
    error: function () {
      var status = $("#job-status").value();
      $("#job-status").text(status + " & Send Failed");
      $("#job-status").css("color", "red");
      $("#send-email").prop("disabled", false);
      alert("ERROR: Could not successfully send client email regarding this job. Try again.");
    }
  });
});

$.ajaxSetup({
  headers: {
    "X-CSRF-Token": $('meta[name="csrf-token"]').attr("content")
  }
});

$("#dismiss-modal").click(function() {
  $(".modal").css("display", "none");
});