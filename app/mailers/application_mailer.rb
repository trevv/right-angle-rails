class ApplicationMailer < ActionMailer::Base
  default from: 'no-reply@rightangleeng.com'
  layout 'mailer'
end
