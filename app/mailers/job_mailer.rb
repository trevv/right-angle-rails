class JobMailer < ApplicationMailer
  require 'open-uri'

  def new_job_email(job)
    @job = job
    for image in @job.images
      attachments["#{image.filename}"] = open(image.service_url).read
    end
    for image in @job.layouts
      attachments["#{image.filename}"] = open(image.service_url).read
    end

    mail(to: "trevor@vertitrev.com", subject: 'New Job: Details')
  end

  def no_city_email(job)
    @job = job
    mail(to: "trevor@vertitrev.com", subject: 'No City for New Job: Details').deliver
  end

  def approved_job_email(job)
    file = job.documents.first
    attachments["#{job.client_last_name}-#{job.company.name}-evaluation-results-#{Date.today}.pdf"] = { mime_type: 'application/pdf', content: file.blob.download }
    puts "Sending approved job email for job: #{job.client_last_name}..."
    puts "Need to send email to client @ #{job.email}"
    mail(to: job.user.email, subject: 'Job Approved: Details').deliver
  end
end
