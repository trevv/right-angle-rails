require "administrate/field/base"

class ImageField < Administrate::Field::Base
  def thumbnail
    data.variant(resize: "100x100")
  end
end
