class JobsController < ApplicationController
  def index
    redirect_to root_path
  end

  def new
    @job = Job.new
    @user = current_user
    @company = current_user.company
    @connection_types = ConnectionType.all
  end

  def create
    @job = Job.new(job_params)
    city = City.find_by(name: params[:job][:locality], state_id: State.find_by(name: params[:job][:administrative_area_level_1]))
    @job.city_id = city.id unless city.nil?
    @job.attributes = {
      city_name: params[:job][:locality],
      street_address: "#{params[:job][:street_number]} #{params[:job][:route]}",
      zip: params[:job][:postal_code],
      state: params[:job][:administrative_area_level_1],
      company: current_user.company,
      user: current_user,
      solar_company_name: current_user.company.name,
      email: current_user.email
    }
    if @job.save
      @job.images.attach(params[:job][:photos]) unless params[:job][:photos].nil?
      @job.roof_material_images.attach(params[:job][:roof_material_images]) unless params[:job][:roof_material_images].nil?
      @job.layouts.attach(params[:job][:layout]) unless params[:job][:layout].nil?
      job_arrays = params[:job_arrays]      
      job_arrays.each do |job_array|
        JobArray.assign_to_job(job_array, @job.id)
      end
      if city.nil?
        flash[:danger] = "We don't currently provide estimates for #{@job.city_name}, but would like to! We've received your project details. We'll reach out to you shortly."
      else
        flash[:success] = "Successfully submitted. We will be in contact soon."
      end
      redirect_to user_path(current_user)
    else
      puts @job.errors.full_messages
      flash[:danger] = @job.errors.full_messages.reverse.to_sentence
      @user = current_user
      render :new
    end
  end

  def show
    @job = Job.find params[:id]
  end

  def generate_report
    @job = Job.find params[:id]
    @estimate = @job.estimate
    @stamp_url = @job.city.stamp.image.service_url

    respond_to do |format|
      format.pdf do
        render pdf: "#{@job.company.name}-#{@job.client_last_name}-#{Date.today}-report",
          template: "documents/letter.pdf.slim",
          viewport_size: '1280x1024',
          locals: {:job => @job}
      end
    end
  end

  private

  def job_params
    params.require(:job).permit(
      :is_premanufacture_truss,
      :email,
      :solar_company_name,
      :client_last_name,
      :street_address,
      :city_name,
      :state,
      :zip,
      :roof_square_footage,
      :home_elevation,
      :roof_pitch,
      :structure_type,
      :number_of_arrays,
      :roof_material,
      :roof_framing_type,
      :company_verified_inspection,
      :connection_type_id,
      :enclosure,
      # :photos,
      # job_arrays: [
      #   :panel_quantity,
      #   :name,
      #   :ballast_weight_psf,
      #   :framing_type,
      #   :beam_span,
      #   :beams_spacing
      # ],
      # :layout => [],
      # :images => []

    )
  end
end
