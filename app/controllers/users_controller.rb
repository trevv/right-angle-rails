class UsersController < ApplicationController
  before_action :authenticate_user!
  after_action :verify_authorized
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  def index
    if !current_user || current_user.role != "admin"
      authorize current_user
    else
      @users = User.all
      authorize current_user
    end
  end

  def show
    @user = current_user
    authorize @user
    if @user.company.nil?
      puts "The user has no company. Let's get that fixed."
      redirect_to new_company_path
    end
  end

  def update
    @user = User.find(params[:id])
    authorize @user
    if @user.update_attributes(secure_params)
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end

  def destroy
    user = User.find(params[:id])
    authorize user
    user.destroy
    redirect_to users_path, :notice => "User deleted."
  end

  private

  def secure_params
    params.require(:user).permit(:role)
  end

  def user_not_authorized
    flash[:alert] = "Oops. Couldn't locate that page."
    redirect_to(request.referrer || root_path)
  end

end
