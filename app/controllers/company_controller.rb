class CompanyController < ApplicationController
  def new
    @company = Company.new
    @user = current_user
  end

  def create
    begin
      if Company.exists?(name: company_params[:name])
        puts "Hey. That company exists already. Add this user to it?"
        redirect_to company_confirm_path(Company.find_by(name: company_params[:name]))
        # current_user.associate_with_company(Company.find_by(name: company_params[:name]).id)
      else
        company = Company.create(
          name: params[:company][:name],
          city: params[:company][:city],
          state: params[:company][:state],
          # letter_type_id: params[:company][:letter_type_id],
          # stamp_on_CAD: params[:company][:stamp_on_CAD],
          # stamp_pages: params[:company][:stamp_pages].tr('[]', '').split(",").map { |s| s.to_i },
          active: true,
        )
        puts "Created company!"
        puts company
        current_user.associate_with_company(company.id)
        redirect_to user_path(current_user), :notice => "Perfect! Thank you! Now you can create as many estimates as you'd like!"
      end

    rescue => e
      puts "ERROR: #{e}"
      redirect_to :back, :alert => "Oops! There was an error creating your company. Try again!"
    end
  end

  def confirm
    @company = Company.find params[:id]
    @user = current_user
  end

  def confirmed
    if params[:confirmed] == "true"
      current_user.associate_with_company(Company.find(params[:id]).id)
      redirect_to user_path(current_user), :notice => "Perfect! Thank you! Now you can create as many estimates as you'd like!"
    else
      redirect_to new_company_path, :alert => "If that wasn't your company, let's try again."
    end
  end

  def index
    @companies = Company.all
    authorize current_user
  end

  def show
    @company = Company.find(params[:id])
  end

  def update
    @company = Company.find(params[:id])

    if @company.update_attributes(secure_params)
      redirect_to company_path, :notice => "company updated."
    else
      redirect_to company_path, :alert => "Unable to update company."
    end
  end

  def destroy
    company = Company.find(params[:id])
    company.destroy
    redirect_to company_path, :notice => "company deleted."
  end

  private

  def company_params
    params.require(:company).permit(:name, :city, :state)
  end
end
