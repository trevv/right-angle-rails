class PhotosController < ApplicationController
  before_action :authenticate_user!

  def show
    @photo = Photo.find params[:id]
    send_file @photo.image.path(params[:style]), disposition: 'inline'
  end
end
