require 'net/http'

module Admin
  class JobsController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:

    def index
      search_term = params[:search].to_s.strip
      resources = Administrate::Search.new(scoped_resource,
                                           dashboard_class,
                                           search_term).run
      resources = apply_resource_includes(resources.order(priority: :asc, created_at: :asc, ))
      resources = order.apply(resources)
      resources = resources.page(params[:page]).per(25)
      page = Administrate::Page::Collection.new(dashboard, order: order)

      render locals: {
        resources: resources,
        search_term: search_term,
        page: page,
        show_search_bar: show_search_bar?,
      }
    end

    def show
      @job = Job.find(params[:id])
      @job_arrays = @job.job_arrays
      puts "This job has #{@job_arrays.count} arrays."
    end

    def generate_report
      @job = Job.find params[:id]

      if @job.documents.count > 0 && params[:fromcache]
        pdf = CombinePDF.parse Net::HTTP.get_response(URI.parse(@job.documents.first.service_url)).body
        respond_to do |format|
          format.pdf do
            send_data pdf.to_pdf, filename: "#{@job.company.name}-#{@job.client_last_name}-#{Date.today}-report", type: "application/pdf"
          end
        end and return
      end

      @job.job_arrays.each do |ja|
        begin
          ja.run_calculation
        rescue => exception
          @exception = exception
          break
          puts "Ooops. There was an error: #{exception}"
        end
      end

      if @exception
        respond_to do |format|
          format.pdf do
            render pdf: "ERROR: #{@job.company.name}-#{@job.client_last_name}-#{Date.today}-report",
              template: "documents/error.pdf.slim",
              viewport_size: '1280x1024',
              locals: {:error => @exception}
            return
          end
        end
      end

      @final_pdf = CombinePDF.new
      @stamp_pdf = CombinePDF.new
      @letter_type = @job.letter_type || @job.company.letter_type || @job.city.letter_type
      @estimate = @job.estimate
      @stamp_url = !@job.city.stamp.nil? ? @job.city.stamp.image.service_url : @job.city.state.stamps.length > 0 ? @job.city.state.stamps.active.first.image.service_url : ""
      all_uploaded_layouts = @job.layouts
      all_uploaded_images = @job.images
      all_uploaded_roof_material_images = @job.roof_material_images

      layout_pdfs = get_attachments_that_are_pdfs(all_uploaded_layouts)
      image_pdfs = get_attachments_that_are_pdfs(all_uploaded_images)
      roof_material_pdfs = get_attachments_that_are_pdfs(all_uploaded_roof_material_images)

      @pdf_ready_images = all_uploaded_images.select { |upload| !upload.blob.content_type.include? "pdf" }
      @pdf_ready_layouts = all_uploaded_layouts.select { |upload| !upload.blob.content_type.include? "pdf" }
      @pdf_ready_roof_material_images = all_uploaded_roof_material_images.select { |upload| !upload.blob.content_type.include? "pdf" }

      report_pdf = render_to_string(
        pdf: "report.pdf",
        template: "documents/letter.pdf.slim",
        viewport_size: '1280x1024',
        locals: {
          :job => @job
        }
      )
      @final_pdf << CombinePDF.parse(report_pdf)

      stamp_pdf = render_to_string(
        pdf: "stamp.pdf",
        template: "documents/stamp.pdf.slim",
        viewport_size: '1280x1024',
        locals: {
          :stamp_url => @stamp_url
        }
      )
      @stamp_pdf << CombinePDF.parse(stamp_pdf).pages[0]

      @page = 0

      if @pdf_ready_images.length > 0
        @pdf_ready_images.each do |image|
          pdf_page = get_image_pdf(image)
        end
      end

      if @pdf_ready_layouts.length > 0
        @pdf_ready_layouts.each do |image|
          pdf_page = get_image_pdf(image)
        end
      end

      if @pdf_ready_roof_material_images.length > 0
        @pdf_ready_roof_material_images.each do |image|
          pdf_page = get_image_pdf(image)
        end
      end

      if layout_pdfs.length > 0
        layout_pdfs.each do |layout|
          layout_pdf = CombinePDF.parse(Net::HTTP.get_response(URI.parse(layout.service_url)).body, {allow_optional_content: true})
          layout_pdf.pages.each {|page| page << @stamp_pdf.pages[0] }
          @final_pdf << layout_pdf
        end
      end

      if image_pdfs.length > 0
        image_pdfs.each do |image|
          image_pdf = CombinePDF.parse(Net::HTTP.get_response(URI.parse(image.service_url)).body, {allow_optional_content: true})
          image_pdf.pages.each {|page| page << @stamp_pdf }
          @final_pdf << image_pdf
        end
      end

      if roof_material_pdfs.length > 0
        roof_material_pdfs.each do |image|
          roof_material_pdf = CombinePDF.parse(Net::HTTP.get_response(URI.parse(image.service_url)).body, {allow_optional_content: true})
          roof_material_pdf.pages.each {|page| page << @stamp_pdf }
          @final_pdf << roof_material_pdf
        end
      end

      @job.documents.attach(io: StringIO.new(@final_pdf.to_pdf), filename: "#{@job.company.name}-#{@job.client_last_name}-#{Date.today}-report.pdf", content_type: "application/pdf")

      respond_to do |format|
        format.pdf do
          send_data @final_pdf.to_pdf, type: 'application/pdf', filename: "#{@job.company.name}-#{@job.client_last_name}-#{Date.today}-report"
        end
      end
    end

    def generate_image
      @job = Job.find params[:id]
      @estimate = @job.estimate
      @stamp_url = @job.city.stamp.image.service_url

      respond_to do |format|
        format.pdf do
          render pdf: "#{@job.company.name}-#{@job.client_last_name}-#{Date.today}-image",
            template: "documents/image.pdf.slim",
            viewport_size: '1280x1024',
            locals: {:job => @job, :stamp_url => @stamp_url}

        end
      end
    end

    def mark_approved
      @job = Job.find params[:id]
      @estimate = @job.estimate

      @job.status = "Approved"
      @job.save!
    end

    def mark_rejected
      @job = Job.find params[:id]
      @job.status = "Rejected"
      @job.save!
    end

    def email_to_client
      job = Job.find params[:id]
      @estimate = job.estimate
      @stamp_url = !job.city.stamp.nil? ? job.city.stamp.image.service_url : job.city.state.stamps.length > 0 ? job.city.state.stamps.active.first.image.service_url : ""
      if job.documents.count == 0
        # generate the letter document

        pdf = render_to_string pdf: "#{job.company.name}-#{job.client_last_name}-#{Date.today}-report",
        template: "documents/letter.pdf.slim", encoding: "UTF-8", header: { right: '[page] of [topage]' }, locals: {:job => job, :stamp_url => @stamp_url, :estimate => @estimate}
        job.documents.attach(io: StringIO.new(pdf), filename: "#{job.company.name}-#{job.client_last_name}-#{Date.today}-report.pdf", content_type: "application/pdf", )
      end
      if job.status.downcase.include? "approved"
        job.status = "Approved & Sent"
        job.save!
        JobMailer.approved_job_email(job).deliver_later
      else
        job.status = "#{job.status} & Sent"
        job.save!
        # JobMailer.rejected_job_email(job)
      end
    end

    def get_attachments_that_are_pdfs(uploads)
      pdfs = []
      uploads.each_with_index do |upload|
        if upload.blob.content_type.include? "pdf"
          pdfs << upload
        end
      end
      return pdfs
    end

    def get_image_pdf(blob)
      @page = @page + 1
      # print_stamp = (@job.company.stamp_on_CAD || @job.city.req_stamp || @job.company.stamp_pages.include?(@page)) ? true : false
      print_stamp = false
      pdf_page = render_to_string(
        pdf: "report.pdf",
        template: "documents/image.pdf.slim",
        viewport_size: '1280x1024',
        locals: {
          :image => blob,
          :stamp_url => @stamp_url,
          :print_stamp => print_stamp
        }
      )
      @final_pdf << CombinePDF.parse(pdf_page)
    end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   Job.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
