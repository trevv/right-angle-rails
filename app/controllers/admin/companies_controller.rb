module Admin
  class CompaniesController < Admin::ApplicationController
    # To customize the behavior of this controller,
    # you can overwrite any of the RESTful actions. For example:
    #
    # def index
    #   super
    #   @resources = Company.
    #     page(params[:page]).
    #     per(10)
    # end

  def create
    begin
      if Company.exists?(name: params[:company][:name])
        puts "Hey. That company exists already. Add this user to it?"
        redirect_to company_confirm_path(Company.find_by(name: params[:company][:name]))
        # current_user.associate_with_company(Company.find_by(name: company_params[:name]).id)
      else
        company = Company.create(
          name: params[:company][:name],
          city: params[:company][:city],
          state: params[:company][:state],
          letter_type_id: params[:company][:letter_type_id],
          stamp_on_CAD: params[:company][:stamp_on_CAD],
          stamp_pages: params[:company][:stamp_pages].tr('[]', '').split(",").map { |s| s.to_i },
          active: params[:company][:active],
        )
        redirect_to admin_companies_path, :notice => "Successfully created company."
      end

    rescue => e
      puts "ERROR: #{e}"
      redirect_to :back, :alert => "Oops! There was an error creating your company. Try again!"
    end
  end

  def update
    company = Company.find(params[:id])
    if company.update_attributes(
          name: params[:company][:name],
          city: params[:company][:city],
          state: params[:company][:state],
          letter_type_id: params[:company][:letter_type_id],
          stamp_on_CAD: params[:company][:stamp_on_CAD],
          stamp_pages: params[:company][:stamp_pages].tr('[]', '').split(",").map { |s| s.to_i },
          active: params[:company][:active],
    )
      redirect_to admin_company_path(company), :notice => "Company updated."
    else
      redirect_to admin_company_path(company), :alert => "Unable to update company."
    end
  end

    # Define a custom finder by overriding the `find_resource` method:
    # def find_resource(param)
    #   Company.find_by!(slug: param)
    # end

    # See https://administrate-prototype.herokuapp.com/customizing_controller_actions
    # for more information
  end
end
