class RegistrationsController < Devise::RegistrationsController
  private

  def sign_up_params
    params.require(:user).permit({ roles: [] }, :name, :email, :password, :password_confirmation, :invitation_token)
  end
end