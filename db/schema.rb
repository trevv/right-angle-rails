# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_08_01_194331) do

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "cities", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.integer "elevation"
    t.integer "ground_snow_load_pressure"
    t.integer "design_wind_speed"
    t.bigint "state_id"
    t.integer "frost_depth"
    t.integer "wind_load"
    t.bigint "stamp_id"
    t.string "ibc"
    t.string "ibc2"
    t.string "iebc"
    t.string "iebc2"
    t.string "five_percent"
    t.string "county"
    t.boolean "req_stamp", default: false
    t.bigint "letter_type_id"
    t.string "asce"
    t.text "notes"
    t.decimal "p_o_variable", precision: 10
    t.decimal "s_o_variable", precision: 10
    t.decimal "a_o_variable", precision: 10
    t.index ["letter_type_id"], name: "index_cities_on_letter_type_id"
    t.index ["stamp_id"], name: "index_cities_on_stamp_id"
    t.index ["state_id"], name: "index_cities_on_state_id"
  end

  create_table "companies", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "state"
    t.string "city"
    t.boolean "stamp_on_CAD", default: true
    t.text "stamp_pages"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "street_address"
    t.string "zip"
    t.bigint "letter_type_id", default: 1
    t.index ["letter_type_id"], name: "index_companies_on_letter_type_id"
  end

  create_table "conditional_ground_snow_loads", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "city_id"
    t.string "comparison"
    t.integer "elevation"
    t.float "ground_snow_load"
    t.index ["city_id"], name: "index_conditional_ground_snow_loads_on_city_id"
  end

  create_table "connection_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.float "shear"
    t.float "pullout"
    t.boolean "active"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "nds_shear"
    t.string "nds_pullout"
  end

  create_table "documents", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pdf_file_name"
    t.string "pdf_content_type"
    t.integer "pdf_file_size"
    t.datetime "pdf_updated_at"
    t.bigint "user_id"
    t.index ["user_id"], name: "index_documents_on_user_id"
  end

  create_table "estimate_arrays", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "estimate_id"
    t.string "name"
    t.integer "number_of_panels"
    t.decimal "slope", precision: 10
    t.decimal "panel_area", precision: 10
    t.decimal "zone1", precision: 10, scale: 4
    t.decimal "zone2", precision: 10, scale: 4
    t.decimal "zone3", precision: 10, scale: 4
    t.decimal "zone1_pressure", precision: 10, scale: 4
    t.decimal "zone2_pressure", precision: 10, scale: 4
    t.decimal "zone3_pressure", precision: 10, scale: 4
    t.decimal "velocity_pressure", precision: 10, scale: 4
    t.decimal "gcpi", precision: 10, scale: 4
    t.decimal "shear_capacity", precision: 10, scale: 4
    t.decimal "shear_trib_area", precision: 10, scale: 4
    t.decimal "pullout_capacity", precision: 10, scale: 4
    t.decimal "pullout_trib_area", precision: 10, scale: 4
    t.string "nds_shear"
    t.string "nds_pullout"
    t.decimal "fs", precision: 10, scale: 4
    t.decimal "beam_span", precision: 10, scale: 4
    t.decimal "spacing", precision: 10, scale: 4
    t.integer "panels_per_rafter"
    t.decimal "panel_distance_from_eave", precision: 10
    t.string "roof_framing_type"
    t.string "orientation"
    t.decimal "bending_moment_without", precision: 10, scale: 4
    t.decimal "bending_moment_with", precision: 10, scale: 4
    t.decimal "bending_moment_percentage", precision: 10, scale: 4
    t.string "bending_moment_105"
    t.decimal "v1_with", precision: 10, scale: 4
    t.decimal "v2_with", precision: 10, scale: 4
    t.decimal "v1_without", precision: 10, scale: 4
    t.decimal "v2_without", precision: 10, scale: 4
    t.decimal "v1_percentage", precision: 10, scale: 4
    t.decimal "v2_percentage", precision: 10, scale: 4
    t.string "v1_105"
    t.string "v2_105"
    t.integer "min_connections"
  end

  create_table "estimates", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "job_id"
    t.integer "company_id"
    t.string "risk_category"
    t.string "wind_exposure_category", default: "C"
    t.decimal "wind_load", precision: 10
    t.string "building_type"
    t.string "roof_material"
    t.decimal "roof_material_psf", precision: 10, scale: 4
    t.string "solar_panel_arrangement"
    t.decimal "solar_panel_psf", precision: 10
    t.decimal "design_wind_speed", precision: 10
    t.decimal "ground_sl", precision: 10, scale: 4
    t.decimal "flat_roof_sl", precision: 10, scale: 4
    t.decimal "slippery_surface", precision: 10, scale: 4
    t.decimal "non_slippery_surface", precision: 10, scale: 4
    t.decimal "sl_without_panels", precision: 10, scale: 4
    t.decimal "sl_with_panels", precision: 10, scale: 4
    t.decimal "dl_without_panels", precision: 10, scale: 4
    t.decimal "dl_with_panels", precision: 10, scale: 4
    t.decimal "dlsl_without_panels", precision: 10, scale: 4
    t.decimal "dlsl_with_panels", precision: 10, scale: 4
    t.decimal "dllr_without_panels", precision: 10, scale: 4
    t.decimal "dllr_with_panels", precision: 10, scale: 4
    t.integer "min_connections"
    t.decimal "roof_ll", precision: 10, scale: 4
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "job_arrays", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "job_id"
    t.string "name"
    t.integer "panel_quantity"
    t.decimal "roof_pitch", precision: 10
    t.boolean "ballast", default: false
    t.decimal "ballast_weight_psf", precision: 10, scale: 4, default: "0.0"
    t.decimal "beam_span", precision: 10, scale: 4, default: "0.0"
    t.decimal "beam_spacing", precision: 10, scale: 4, default: "0.0"
    t.string "framing_type"
    t.boolean "framing_type_is_other", default: false
    t.string "framing_type_other"
    t.integer "number_of_panels_on_rafter"
    t.decimal "panel_distance_from_eave", precision: 10, scale: 4
    t.string "orientation"
    t.string "video_link"
    t.index ["job_id"], name: "index_job_arrays_on_job_id"
  end

  create_table "jobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.boolean "send_pdf", default: false, null: false
    t.boolean "is_premanufacture_truss", default: false, null: false
    t.string "email"
    t.string "solar_company_name"
    t.string "client_last_name"
    t.string "street_address"
    t.string "city_name"
    t.string "state"
    t.integer "zip"
    t.decimal "roof_square_footage", precision: 10
    t.integer "home_elevation"
    t.decimal "roof_pitch", precision: 10
    t.integer "company_id"
    t.integer "city_id"
    t.string "structure_type"
    t.integer "number_of_arrays"
    t.string "roof_material"
    t.string "roof_framing_type"
    t.boolean "company_verified_inspection", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "user_id"
    t.string "enclosure"
    t.string "status", default: "In Review"
    t.integer "priority"
    t.bigint "letter_type_id"
    t.text "notes"
    t.bigint "connection_type_id"
    t.index ["connection_type_id"], name: "index_jobs_on_connection_type_id"
    t.index ["letter_type_id"], name: "index_jobs_on_letter_type_id"
    t.index ["user_id"], name: "index_jobs_on_user_id"
  end

  create_table "letter_types", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "paragraph1"
    t.text "paragraph2"
    t.text "paragraph3"
    t.text "notes"
    t.text "subject"
  end

  create_table "photos", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "job_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "stamps", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "file_path"
    t.boolean "active", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "state_id"
    t.index ["state_id"], name: "index_stamps_on_state_id"
  end

  create_table "states", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "abbrev"
    t.integer "elevation"
    t.integer "ground_snow_load_pressure"
    t.integer "design_wind_speed"
    t.text "ibc"
    t.text "iebc"
    t.text "ibc2"
    t.text "iebc2"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "five_percent"
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "role"
    t.bigint "company_id"
    t.string "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer "invitation_limit"
    t.string "invited_by_type"
    t.bigint "invited_by_id"
    t.integer "invitations_count", default: 0
    t.index ["company_id"], name: "index_users_on_company_id"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true
    t.index ["invitations_count"], name: "index_users_on_invitations_count"
    t.index ["invited_by_id"], name: "index_users_on_invited_by_id"
    t.index ["invited_by_type", "invited_by_id"], name: "index_users_on_invited_by_type_and_invited_by_id"
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "cities", "letter_types"
  add_foreign_key "cities", "stamps"
  add_foreign_key "cities", "states"
  add_foreign_key "companies", "letter_types"
  add_foreign_key "conditional_ground_snow_loads", "cities"
  add_foreign_key "documents", "users"
  add_foreign_key "job_arrays", "jobs"
  add_foreign_key "jobs", "connection_types"
  add_foreign_key "jobs", "letter_types"
  add_foreign_key "jobs", "users"
  add_foreign_key "stamps", "states"
  add_foreign_key "users", "companies"
end
