# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

company = Company.create({
  name: "Vertitrev Development",
  state: "Utah",
  city: "Lehi",
  street_address: "2068 N Hiddencreek Dr",
  zip: "84043",
  stamp_on_CAD: true,
  stamp_pages: [1,2,3],
  active: true
})
company.save!

user = CreateAdminService.new.call

puts 'CREATED ADMIN USER: ' << user.email
state = State.create({
  name: 'Utah',
  abbrev: 'UT',
  elevation: 4900,
  ground_snow_load_pressure: 500,
  design_wind_speed: 189,
  ibc: "2016 Utah Building Code",
  iebc: "2016 Utah Existing Building Code",
  ibc2: "2016 UBC 1607.12.5",
  iebc2: "2016 Utah Existing Building Code (UEBC) 2016 section 403.3",
})
state.save!

city = City.create({
  name: 'Provo',
  state_id: State.first.id,
  elevation: 4900,
  ground_snow_load_pressure: 500,
  design_wind_speed: 189,
  wind_load: 100
})

city.save!

