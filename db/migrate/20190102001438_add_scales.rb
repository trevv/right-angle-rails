class AddScales < ActiveRecord::Migration[5.2]
  def change
    change_column :estimates, :roof_material_psf, :decimal, scale: 4, precision: 10
    change_column :estimates, :ground_sl, :decimal, scale: 4, precision: 10
    change_column :estimates, :flat_roof_sl, :decimal, scale: 4, precision: 10
    change_column :estimates, :slippery_surface, :decimal, scale: 4, precision: 10
    change_column :estimates, :non_slippery_surface, :decimal, scale: 4, precision: 10
    change_column :estimates, :sl_without_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :sl_with_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dl_with_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dl_without_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dlsl_without_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dlsl_with_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dllr_with_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :dllr_without_panels, :decimal, scale: 4, precision: 10
    change_column :estimates, :roof_ll, :decimal, scale: 4, precision: 10

    change_column :estimate_arrays, :zone1, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :zone2, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :zone3, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :zone1_pressure, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :zone2_pressure, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :zone3_pressure, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :velocity_pressure, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :gcpi, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :shear_capacity, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :shear_trib_area, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :pullout_capacity, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :pullout_trib_area, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :fs, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :beam_span, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :spacing, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :bending_moment_with, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :bending_moment_without, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :bending_moment_percentage, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v1_with, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v2_with, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v1_without, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v2_without, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v1_percentage, :decimal, scale: 4, precision: 10
    change_column :estimate_arrays, :v2_percentage, :decimal, scale: 4, precision: 10

    change_column :job_arrays, :ballast_weight_psf, :decimal, scale: 4, precision: 10
    change_column :job_arrays, :beam_span, :decimal, scale: 4, precision: 10
    change_column :job_arrays, :beam_spacing, :decimal, scale: 4, precision: 10
    change_column :job_arrays, :panel_distance_from_eave, :decimal, scale: 4, precision: 10

  end
end
