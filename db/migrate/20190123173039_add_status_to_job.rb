class AddStatusToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :status, :string, default: "In Review"
  end
end
