class AddDefaultsToJobArrays < ActiveRecord::Migration[5.2]
  def change
    change_column :job_arrays, :ballast_weight_psf, :decimal, precision: 10,  default: 0
    change_column :job_arrays, :beam_span, :decimal, precision: 10,  default: 0
    change_column :job_arrays, :beam_spacing, :decimal, precision: 10,  default: 0
  end
end
