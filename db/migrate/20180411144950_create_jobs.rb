class CreateJobs < ActiveRecord::Migration[5.2]
  def change
    create_table :jobs do |t|
      # relational fields
      t.integer   :city_id

      # non-calculated fields #
      t.boolean   :send_pdf, null: false, default: false
      t.boolean   :is_premanufacture_truss, null: false, default: false
      t.string    :email
      t.string    :solar_company_name
      t.string    :client_last_name
      t.string    :street_address
      t.string    :city_name
      t.string    :state
      t.integer   :zip
      t.decimal   :roof_square_footage
      t.integer   :home_elevation
      t.integer   :number_of_panels
      t.decimal   :roof_pitch

      # pull from city fields #
      t.string    :risk_category
      t.integer   :city_elevation
      t.string    :wind_exposure
      t.decimal   :design_wind_speed
      t.decimal   :ground_snow_load_pressure
      t.decimal   :roof_dead_load_pressure
      t.decimal   :solar_array_load_pressure

      # calculated fields #
      t.decimal   :total_solar_array_area
      t.decimal   :roof_dead_load_pounds
      t.decimal   :roof_snow_load_pressure
      t.decimal   :roof_snow_load_pounds
      t.decimal   :solar_array_load_pounds
      t.decimal   :roof_stress_percent
      t.decimal   :wind_pressure
      t.decimal   :tributary_area_per_connection
      t.decimal   :min_embedment_lag_screw
      t.decimal   :lag_screw_pullout_capacity
      t.decimal   :lag_screw_shear_capacity
      t.decimal   :minimum_number_of_connections

      t.timestamps
    end
  end
end
