class AddNotesToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :notes, :text
  end
end
