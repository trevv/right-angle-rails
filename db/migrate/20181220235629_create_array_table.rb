class CreateArrayTable < ActiveRecord::Migration[5.2]
  def change
    create_table :array_tables do |t|
      t.references :job, foreign_key: true
      t.string :name
      t.integer :panel_quantity
      t.decimal :roof_pitch
      t.boolean :ballast, default: false
      t.decimal :ballas_weight_psf
      t.decimal :beam_span
      t.decimal :beam_spacing
      t.string :framing_type
      t.boolean :framing_type_is_other, default: false
      t.string :framing_type_other
      t.integer :number_of_panels_on_rafter
      t.decimal :panel_distance_from_eave
      t.string :orientation
    end
  end
end
