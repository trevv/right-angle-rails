class AddMinConnectionsToEstimateArrays < ActiveRecord::Migration[5.2]
  def change
    add_column :estimate_arrays, :min_connections, :integer
  end
end
