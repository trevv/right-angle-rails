class RemoveArrayColumnsFromJobsTable < ActiveRecord::Migration[5.2]
  def change
    remove_column :jobs, :number_of_panels
    remove_column :jobs, :average_ballast_psf
    remove_column :jobs, :rafter_truss_span
    remove_column :jobs, :rafter_truss_spacing
    remove_column :jobs, :number_of_footing
    remove_column :jobs, :pole_size
    remove_column :jobs, :orientation
    remove_column :jobs, :number_of_panels_on_rafter
    remove_column :jobs, :panel_distance_from_eave
    rename_column :array_tables, :ballas_weight_psf, :ballast_weight_psf
    rename_table :array_tables, :arrays
  end
end
