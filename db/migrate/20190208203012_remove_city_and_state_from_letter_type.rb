class RemoveCityAndStateFromLetterType < ActiveRecord::Migration[5.2]
  def change
    remove_column :letter_types, :city_id
    remove_column :letter_types, :state_id
    remove_column :letter_types, :file_path

  end
end
