class CreateConditionalElevationSnowLoadTable < ActiveRecord::Migration[5.2]
  def change
    create_table :conditional_ground_snow_loads do |t|
      t.references :city, foreign_key: true
      t.string :comparison
      t.integer :elevation
      t.float :ground_snow_load

    end
  end
end

