class AddParagraphsToLetterType < ActiveRecord::Migration[5.2]
  def change
    add_column :letter_types, :paragraph1, :text
    add_column :letter_types, :paragraph2, :text
    add_column :letter_types, :paragraph3, :text
    add_column :letter_types, :notes, :text
  end
end
