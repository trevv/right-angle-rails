class AddReqStampToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :req_stamp, :boolean, default: false
  end
end
