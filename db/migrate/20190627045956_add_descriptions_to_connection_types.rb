class AddDescriptionsToConnectionTypes < ActiveRecord::Migration[5.2]
  def change
    add_column :connection_types, :nds_shear, :string
    add_column :connection_types, :nds_pullout, :string
  end
end
