class CreateCompanies < ActiveRecord::Migration[5.2]
  def change
    create_table :companies do |t|
      t.string :name
      t.string :state
      t.string :city
      t.boolean :stamp_on_CAD, default: true
      t.text :stamp_pages
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
