class CreateLetterTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :letter_types do |t|
      t.string :name
      t.string :file_path
      t.references :city, foreign_key: true
      t.references :state, foreign_key: true

      t.timestamps
    end
  end
end
