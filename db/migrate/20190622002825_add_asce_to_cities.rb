class AddAsceToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :asce, :string
  end
end
