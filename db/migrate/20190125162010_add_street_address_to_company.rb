class AddStreetAddressToCompany < ActiveRecord::Migration[5.2]
  def change
    add_column :companies, :street_address, :text
  end
end
