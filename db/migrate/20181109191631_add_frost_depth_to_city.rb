class AddFrostDepthToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :frost_depth, :integer
  end
end
