class CreateEstimate < ActiveRecord::Migration[5.2]
  def change
    create_table :estimates do |t|
      # relational fields
      t.integer   :job_id
      t.integer   :company_id
      # pull from city fields #
      t.string    :risk_category
      t.integer   :city_elevation
      t.string    :wind_exposure
      t.decimal   :design_wind_speed
      t.decimal   :ground_snow_load_pressure
      t.decimal   :roof_dead_load_pressure
      t.decimal   :solar_array_load_pressure

      # calculated fields #
      t.decimal   :total_solar_array_area
      t.decimal   :roof_dead_load_pounds
      t.decimal   :roof_snow_load_pressure
      t.decimal   :roof_snow_load_pounds
      t.decimal   :solar_array_load_pounds
      t.decimal   :roof_stress_percent
      t.decimal   :wind_pressure
      t.decimal   :tributary_area_per_connection
      t.decimal   :min_embedment_lag_screw
      t.decimal   :lag_screw_pullout_capacity
      t.decimal   :lag_screw_shear_capacity
      t.decimal   :minimum_number_of_connections

      t.timestamps
    end
  end
end
