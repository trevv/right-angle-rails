class AddSubjectToLetterType < ActiveRecord::Migration[5.2]
  def change
    add_column :letter_types, :subject, :text
  end
end
