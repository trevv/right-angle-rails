class AddCountyToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :county, :string
  end
end
