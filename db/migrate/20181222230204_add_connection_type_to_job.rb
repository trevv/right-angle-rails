class AddConnectionTypeToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :connection_type, :string
  end
end
