class UpdateStateColumns < ActiveRecord::Migration[5.2]
  def change
    rename_column :states, :ibc1, :ibc
    add_column :states, :five_percent, :string
  end
end
