class CreateEstimatesTable < ActiveRecord::Migration[5.2]
  def change
    create_table :estimates do |t|
      # relational fields
      t.integer   :job_id
      t.integer   :company_id
      # pull from city fields #
      t.string    :risk_category
      t.string    :wind_exposure_category, default: "C"
      t.decimal   :wind_load, precision: 10
      t.string    :building_type

      t.string    :roof_material
      t.decimal   :roof_material_psf, precision: 10
      t.string    :solar_panel_arrangement
      t.decimal   :solar_panel_psf, precision: 10
      t.decimal   :design_wind_speed, precision: 10
      t.decimal   :ground_sl, precision: 10
      t.decimal   :flat_roof_sl, precision: 10

      t.decimal   :slippery_surface, precision: 10
      t.decimal   :non_slippery_surface, precision: 10

      t.decimal   :sl_without_panels, precision: 10
      t.decimal   :sl_with_panels, precision: 10

      t.decimal   :dl_without_panels, precision: 10
      t.decimal   :dl_with_panels, precision: 10

      t.decimal   :dlsl_without_panels, precision: 10
      t.decimal   :dlsl_with_panels, precision: 10

      t.decimal   :dllr_without_panels, precision: 10
      t.decimal   :dllr_with_panels, precision: 10
      t.integer   :min_connections
      t.decimal   :roof_ll, precision: 10
      t.timestamps

    end
  end
end
