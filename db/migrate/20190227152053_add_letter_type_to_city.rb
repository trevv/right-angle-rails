class AddLetterTypeToCity < ActiveRecord::Migration[5.2]
  def change
    add_reference :cities, :letter_type, foreign_key: true
  end
end
