class CreateCities < ActiveRecord::Migration[5.2]
  def change
    create_table :cities do |t|
      t.string   :name
      t.string   :state
      t.string   :state_abbreviation
      t.integer  :elevation
      t.integer  :ground_snow_load_pressure
      t.integer  :design_wind_speed
    end
  end
end
