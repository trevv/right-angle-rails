class AddNotesToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :notes, :text
  end
end
