class ChangeJobTable < ActiveRecord::Migration[5.2]
  def up
    drop_table :jobs
  end

  def down
    create_table :jobs do |t|
      t.boolean   :send_pdf, null: false, default: false
      t.boolean   :is_premanufacture_truss, null: false, default: false
      t.string    :email
      t.string    :solar_company_name
      t.string    :client_last_name
      t.string    :street_address
      t.string    :city_name
      t.string    :state
      t.integer   :zip
      t.decimal   :roof_square_footage
      t.integer   :home_elevation
      t.integer   :number_of_panels
      t.decimal   :roof_pitch

      # relational fields
      t.integer   :company_id
      t.integer   :city_id

      # inputs from form
      t.string    :structure_type
      t.integer   :number_of_arrays
      t.decimal   :average_ballast_psf
      t.string    :roof_material
      t.string    :roof_framing_type
      t.decimal   :rafter_truss_span
      t.decimal   :rafter_truss_spacing
      t.integer   :number_of_footing
      t.string    :pole_size
      t.boolean   :company_verified_inspection, default: false

      t.timestamps
    end

  end
end
