class AddAttachmentsToDocuments < ActiveRecord::Migration[5.2]
  def change
    add_column :documents, :pdf_file_name, :string
    add_column :documents, :pdf_content_type, :string
    add_column :documents, :pdf_file_size, :integer
    add_column :documents, :pdf_updated_at, :datetime
  end
end
