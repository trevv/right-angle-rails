class CreateStates < ActiveRecord::Migration[5.2]
  def change
    create_table :states do |t|
      t.string :name
      t.string :abbrev
      t.integer :elavation
      t.integer :ground_snow_load_pressure
      t.integer :design_wind_speed
      t.text :ibc1
      t.text :iebc
      t.text :ibc2
      t.text :iebc2

      t.timestamps
    end
  end
end
