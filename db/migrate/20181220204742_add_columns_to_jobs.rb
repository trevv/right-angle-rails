class AddColumnsToJobs < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :number_of_panels_on_rafter, :integer
    add_column :jobs, :panel_distance_from_eave, :decimal
    add_column :jobs, :orientation, :string
  end
end
