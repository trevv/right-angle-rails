class ChangeConnectionTypeOnJobs < ActiveRecord::Migration[5.2]
  def change
    remove_column :jobs, :connection_type
    add_reference :jobs, :connection_type, foreign_key: true
  end
end
