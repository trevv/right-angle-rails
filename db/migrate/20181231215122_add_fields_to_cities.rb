class AddFieldsToCities < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :ibc, :string
    add_column :cities, :ibc2, :string
    add_column :cities, :iebc, :string
    add_column :cities, :iebc2, :string
    add_column :cities, :five_percent, :string
  end
end
