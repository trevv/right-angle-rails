class AddUtahSpecificSnowLoadToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :p_o_variable, :decimal
    add_column :cities, :s_o_variable, :decimal
    add_column :cities, :a_o_variable, :decimal
  end
end
