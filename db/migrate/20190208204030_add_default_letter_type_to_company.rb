class AddDefaultLetterTypeToCompany < ActiveRecord::Migration[5.2]
  def change
    add_reference :companies, :letter_type, foreign_key: true
  end
end
