class AddWindloadToCity < ActiveRecord::Migration[5.2]
  def change
    add_column :cities, :wind_load, :integer
  end
end
