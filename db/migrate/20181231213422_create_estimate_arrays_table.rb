class CreateEstimateArraysTable < ActiveRecord::Migration[5.2]
  def change
    create_table :estimate_arrays do |t|
      t.integer :estimate_id
      t.string :name
      t.integer :number_of_panels
      t.decimal :slope, precision: 10
      t.decimal :panel_area, precision: 10
      t.decimal :zone1, precision: 10
      t.decimal :zone2, precision: 10
      t.decimal :zone3, precision: 10
      t.decimal :zone1_pressure, precision: 10
      t.decimal :zone2_pressure, precision: 10
      t.decimal :zone3_pressure, precision: 10
      t.decimal :velocity_pressure, precision: 10
      t.decimal :gcpi, precision: 10
      t.decimal :shear_capacity, precision: 10
      t.decimal :shear_trib_area, precision: 10
      t.decimal :pullout_capacity, precision: 10
      t.decimal :pullout_trib_area, precision: 10
      t.string :nds_shear
      t.string :nds_pullout
      t.decimal :fs, precision: 10
      t.decimal :beam_span, precision: 10
      t.decimal :spacing, precision: 10
      t.integer :panels_per_rafter
      t.decimal :panel_distance_from_eave, precision: 10
      t.string :roof_framing_type
      t.string :orientation

      t.decimal :bending_moment_without, precision: 10
      t.decimal :bending_moment_with, precision: 10
      t.decimal :bending_moment_percentage, precision: 10
      t.string :bending_moment_105
      t.decimal :v1_with, precision: 10
      t.decimal :v2_with, precision: 10
      t.decimal :v1_without, precision: 10
      t.decimal :v2_without, precision: 10
      t.decimal :v1_percentage, precision: 10
      t.decimal :v2_percentage, precision: 10
      t.string :v1_105
      t.string :v2_105
    end
  end
end
