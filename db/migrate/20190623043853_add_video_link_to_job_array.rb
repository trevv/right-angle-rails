class AddVideoLinkToJobArray < ActiveRecord::Migration[5.2]
  def change
    add_column :job_arrays, :video_link, :string
  end
end
