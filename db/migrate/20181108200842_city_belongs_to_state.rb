class CityBelongsToState < ActiveRecord::Migration[5.2]
  def change
    remove_column :cities, :state
    remove_column :cities, :state_abbreviation
    add_reference :cities, :state, foreign_key: true
  end
end
