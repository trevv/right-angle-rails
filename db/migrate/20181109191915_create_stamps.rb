class CreateStamps < ActiveRecord::Migration[5.2]
  def change
    create_table :stamps do |t|
      t.string :name
      t.string :file_path
      t.boolean :active, default: true

      t.timestamps
    end
  end
end
