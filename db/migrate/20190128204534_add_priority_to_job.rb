class AddPriorityToJob < ActiveRecord::Migration[5.2]
  def change
    add_column :jobs, :priority, :integer
  end
end
