class AddDefaultLetterTypeToJob < ActiveRecord::Migration[5.2]
  def change
    add_reference :jobs, :letter_type, foreign_key: true
  end
end
