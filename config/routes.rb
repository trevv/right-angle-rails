Rails.application.routes.draw do
  root to: 'visitors#index'

  namespace :admin do
    resources :jobs
    resources :job_arrays
    resources :users
    resources :companies
    resources :cities
    resources :states
    resources :connection_types
    resources :letter_types
    resources :stamps
    resources :conditional_ground_snow_loads

    root to: "users#index"

    get 'job/:id/generate_report', to: 'jobs#generate_report', as: :generate_report
    get 'job/:id/generate_image', to: 'jobs#generate_image', as: :generate_image
    get 'job/:id/email_to_client', to: 'jobs#email_to_client', as: :email_to_client
    patch 'job/:id/mark_approved', to: 'jobs#mark_approved', as: :mark_approved
    patch 'job/:id/mark_rejected', to: 'jobs#mark_rejected', as: :mark_rejected
  end

  devise_for :users, :controllers => { registrations: 'registrations' }
  resources :users
  resources :jobs
  resources :company

  get 'job/:id/generate_report', to: 'jobs#generate_report', as: :generate_report
  get '/company/:id/confirm', to: "company#confirm", as: :company_confirm
  get '/company/:id/confirmed', to: "company#confirmed", as: :company_confirmed
end
