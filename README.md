Right Angle Rails
================

A rails project built to automate the estimate generation process for Right Angle Engineering.

This application requires:

- Ruby 2.5.1
- Rails 5.2.1


Getting Started
---------------

Documentation and Support
-------------------------

Issues
-------------

Contributing
------------
@trevv - trevor@vertitrev.com